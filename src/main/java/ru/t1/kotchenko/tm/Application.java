package ru.t1.kotchenko.tm;

import static ru.t1.kotchenko.tm.constant.TerminalConst.*;

public class Application {

    public static void main(String[] args) {
        parseArguments(args);
    }

    private static void parseArguments(final String[] args) {
        if (args == null || args.length == 0) showWelcome();
        final String arg = args[0];
        parseArgument(arg);
    }

    private static void parseArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case VERSION:
                showVersion();
                break;
            case HELP:
                showHelp();
                break;
            case INFO:
                showDeveloperInfo();
                break;
            default:
                showWelcome();
        }
    }

    private static void showWelcome() {
        System.out.println("*** WELCOME TO TASK MANAGER ***");
        System.exit(0);
    }

    public static void showVersion() {
        System.out.println("[VERSION]");
        System.out.println("1.3.0");
    }

    public static void showHelp() {
        System.out.println("[HELP]");
        System.out.printf("%s - show application version. \n", VERSION);
        System.out.printf("%s - show application commands. \n", HELP);
        System.out.printf("%s - show developer info. \n", INFO);
    }

    public static void showDeveloperInfo() {
        System.out.println("[DEVELOPER]");
        System.out.println("NAME: Ivan Kotchenko");
        System.out.println("E-MAIL: IvanKotchenko@yandex.ru");
    }

}